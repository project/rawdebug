<?php

/**
 * @file
 * Debugging functions.
 */

use Drupal\Core\Entity\EntityBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The location of the log file. See README.md.
 */
if (!defined( 'ABSPATH' )) {
  $GLOBALS['rawdebug_log_file_full_path'] = __DIR__ . '/files/log.log';
}
else {
  $GLOBALS['rawdebug_log_file_full_path'] = __DIR__ . '/wp-content/uploads/log.log';
}

/**
 * Whether to log to error_log if the log file is unavailable. See README.md.
 */
$GLOBALS['rawdebug_use_error_log_as_backup'] = FALSE;

if (defined( 'ABSPATH' )) {
  function t($string, $args) {
    return strtr($string, $args);
  }
}

if (!function_exists('dbt')) {
  /**
   * Return a backtrace.
   *
   * @param int $limit
   *   How many levels to return.
   * @param bool $withArgs
   *   Whether to include the arguments in the return value.
   *   Setting this to TRUE isn't recommended, it's better to use
   *   summerize on individual arguments to conserve RAM.
   *
   * @return array
   *   The calls in reverse chronological order.
   */
  function dbt($limit = 11, $withArgs = FALSE) {
    $ary = debug_backtrace(0, $limit);
    if ($withArgs) {
      return $ary;
    }

    foreach ($ary as $k => $v) {
      unset($v['args']);
      $ary[$k] = $v;
    }

    return $ary;
  }
}

if (!function_exists('summerize')) {
  /**
   * Return the summary of a variable.
   *
   * @param mixed $arg
   *   The variable to summarize.
   *
   * @return string|array
   *   The summary.
   */
  function summerize($arg) {
    if (!$arg) {
      return '[NULL]';
    }

    if (is_scalar($arg)) {
      return '' . $arg;
      // return '' . $arg . ' (' . gettype($arg) . ')';
    }

    if (is_array($arg)) {
      $ret = [];
      foreach ($arg as $k => $v) {
        $ret[$k] = summerize($v);
      }

      return $ret;
    }

    $clz = get_class($arg);
    if ($arg instanceof EntityBase) {
      return $clz . ' (' . $arg->id() . ', ' . $arg->bundle() . ', ' . $arg->label() . ')';
    }
    else if ($arg instanceof TranslatableMarkup) {
      $s = '' . $arg;
      if (mb_strlen($s) > 100) {
        $s = mb_substr($s, 0, 100) . '...';
      }
      return '[TM] ' . $s;
    }
    else if ($arg instanceof WP_Block && isset($arg->parsed_block)) {
      return $clz . ' (' . json_encode($arg->parsed_block) . ')';
    }
    else if ($arg instanceof WP_Comment && isset($arg->comment_ID) && isset($arg->comment_post_ID)) {
      return $clz . ' (' . $arg->comment_ID . ' on ' . $arg->comment_post_ID . ')';
    }
    else if ($arg instanceof WP_Error) {
      return $clz . ' (' . $arg->get_error_message() . ')';
    }
    else if ($arg instanceof WP_Query && isset($arg->query)) {
      return $clz . ' (' . json_encode($arg->query) . ')';
    }
    else if ($arg instanceof WP_Role && isset($arg->name)) {
      return $clz . ' (' . $arg->name . ')';
    }
    else if ($arg instanceof WP_Term && isset($arg->term_id) && isset($arg->name)) {
      return $clz . ' (' . $arg->term_id . ', ' . $arg->name . ')';
    }
    else if ($arg instanceof WP_User && isset($arg->ID)) {
      return $clz . ' (' . $arg->ID . ')';
    }
    else if ($arg instanceof WP_Widget && isset($arg->name)) {
      return $clz . ' (' . $arg->name . ')';
    }

    return $clz;
  }
}

if (!function_exists('rawdebug')) {
  /**
   * Log zero or more variables to a file.
   *
   * @param mixed $args
   *   The variables to log.
   */
  function rawdebug(...$args) {
    $values = [];

    foreach ($args as $arg) {
      $val = summerize($arg);
      if (!is_scalar($val)) {
        $val = print_r($val, TRUE);
      }
      $values[] = $val;
    }

    $values = implode(' ', $values) . "\n";

    $full_path = !empty($GLOBALS['rawdebug_log_file_full_path']) ? $GLOBALS['rawdebug_log_file_full_path'] : NULL;
    if (!$full_path) {
      error_log('rawdebug error: full_path is empty. Requested output: ' . $values);
      return;
    }

    $fp = fopen($full_path, 'a');
    if (!$fp) {
      if (!empty($GLOBALS['rawdebug_use_error_log_as_backup'])) {
        error_log($values);
        return;
      }

      $exists = file_exists($full_path);

      if ($exists) {
        echo t('rawdebug warning: the log file @file exists but could not be opened for writing. See the "file is not writeable" section of the rawdebug README.md file', ['@file' => $full_path]);
      }
      else {
        echo t('rawdebug warning: the log file @file could not be created. See the "file cannot be created" section of the rawdebug README.md file', ['@file' => $full_path]);
      }

      exit;
    }

    fwrite($fp, $values);

    fclose($fp);
  }
}
